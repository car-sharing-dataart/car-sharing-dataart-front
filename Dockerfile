FROM node:15.13-alpine
WORKDIR /car-sharing-dataart-front
ENV PATH="./node_modules/.bin:$PATH"
COPY . .
RUN npm run build