# **CAR-SHARING-DATAART(FRONT)**

**This project is created for improving skills and demonstration
of developer proficiency.**

**The main purposes of creation are:**

- give car-sharing functionality for users;
- to provide easy to understand application for deals which include temporary usage of another user`s car for money compensation;
- fast and high-quality solution of disputable situations between users;
- ensuring the confidentiality of communication between users.

**To start the project you need:**

- Type in command line from root folder "npm start" and wait for start;
- Wait until you will be redirected from VS Code to your default browser with url: "http:/localhost:3000/home"; 

**Technologies:**

- React Js
- Boostrap
- HTML, CSS

###### Mentor: Alexander Yushko

###### Developer: Denys Suk