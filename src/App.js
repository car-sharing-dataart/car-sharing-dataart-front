import React from "react";
import Header from "./components/header";
import 'bootstrap/dist/css/bootstrap.min.css';
import Cars from './components/templates/cars.js';
import SingUp from "./components/templates/sign-up.js";
import LogIn from "./components/templates/login.js";
import AddCar from "./components/templates/add-car";

import { 
  BrowserRouter as Router,
  Switch,
  Route
 } 
from "react-router-dom";

import  Home  from './components/templates/home';
import  Profile  from './components/templates/profile';
import authService from "./components/service/auth/auth-service";



function App() {
    const user = authService.getCurrentUser();
    return (
    <>
      <Router>
      <Header/>
      <Switch>
        <Route exact path="/home" component={Home}/>
        <Route exact path="/profile" component={Profile}/>
          {user && user.verified === true && (
        <Route exact path="/add-car" component={AddCar}/>
          )}
        <Route exact path="/cars" component={Cars}/>
          {!user &&(
        <Route exact path="/singup" component={SingUp}/>
          )} {!user &&(
        <Route exact path="/login" component={LogIn}/>
          )}
      </Switch>
      </Router> 
    </>

  );
}

export default App;
