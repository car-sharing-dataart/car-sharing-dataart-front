import React from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import authService from "./service/auth/auth-service";

export default function Header() {
  const user = authService.getCurrentUser();
            return (
              <Navbar expand="lg" bg="dark" variant="dark">
              <Navbar.Brand style={{ "margin-left": "20px", fontFamily: "Cambria" }}>
                Car Sharing DataArt
              </Navbar.Brand>
              <Nav style={{ "margin-left": "790px" }}>
                <Nav.Link>
                  <Link to="/home">
                    <Button variant="outline-primary">Home</Button>
                  </Link>
                </Nav.Link>
                <Nav.Link>
                  <Link to="/profile">
                    <Button variant="outline-primary">Profile</Button>
                  </Link>
                </Nav.Link>
                {user && user.verified === true && (
                <Nav.Link>
                  <Link to="/add-car">
                    <Button variant="outline-primary">Add car</Button>
                  </Link>
                </Nav.Link>
                )}
                <Nav.Link>
                  <Link to="/cars">
                    <Button variant="outline-primary">Cars</Button>
                  </Link>
                </Nav.Link>
                {!user &&(
                <Nav.Link>
                <Link to="/singup">
                    <Button variant="outline-primary">Sign up</Button>
                  </Link>
                </Nav.Link>
                )} {!user &&(
                <Nav.Link>
                  <Link to="/login">
                    <Button variant="outline-primary">Log in</Button>
                  </Link>
                </Nav.Link>
                )}
                {user &&(
                <Nav.Link>
                <Link to="/login">
                <Button variant="outline-primary" onClick={authService.logout}>Log out</Button>
                </Link>
                </Nav.Link>
                )}
              </Nav>
            </Navbar>
    );
  }

