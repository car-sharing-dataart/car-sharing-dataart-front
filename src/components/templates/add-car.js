import React, { Component } from "react";
import CarService from "../service/car-service";
import axios from "axios";
import AuthService from "../service/auth/auth-service";
import moment from "moment";
import { FormErrors } from "./form-errors";

class AddCar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            brand: "",
            model: "",
            number: "",
            vin: "",
            year: "",
            colour: "",
            type: "",
            city: "",
            images: [{
                url: ""
            }],
            userId: "",

            formErrors: {
                brand: "",
                model: "",
                number: "",
                vin: "",
                year: "",
                colour: "",
                type: "",
                city: "",
            },

            service: true,
            resp: true,
            resp500: true,
            resp400: true,
            message: '',
            imagesUploaded: true,
            userHasToken: true,

            brandValid: false,
            modelValid: false,
            numberValid: false,
            vinValid: false,
            yearValid: false,
            colourValid: false,
            typeValid: false,
            cityValid: false,
            formValid: false,
        };
        this.saveCar = this.saveCar.bind(this);
    }

    replaceItems(){
        const images = sessionStorage.getItem("imageData");
        let imageToParse = images.split(",");
        const readyUrls = [];
        for (let i = 0; i < imageToParse.length; i++) {
            imageToParse[i] = imageToParse[i].replace("image" + i, '');
            imageToParse[i] = imageToParse[i].replace(":", '');
            imageToParse[i] = imageToParse[i].replace("{", '');
            imageToParse[i] = imageToParse[i].replace("}", '');
            imageToParse[i] = imageToParse[i].replace("\"\"\"", '');
            imageToParse[i] = imageToParse[i].replace("\"", '');
            readyUrls.push({
                url: imageToParse[i]
            });
        }
        return readyUrls;
    }

    saveCar = (save) => {
        save.preventDefault();
        const currentUser = AuthService.getCurrentUser();
        this.setState({ currentUser: currentUser})
        if(this.state.my_file == null) {
            this.setState({imagesUploaded: false});
        }else if (currentUser == null){
            this.setState({userHasToken: false});
        }else {
            const fileData = new FormData();
            fileData.append("UPLOADCARE_PUB_KEY", '7498ac6ea096b33d84d8');
            fileData.append("UPLOADCARE_STORE", 'auto');
            for (let i = 0; i < this.state.my_file.length; i++) {
                fileData.append(`image` + i, this.state.my_file[i])
            }
            axios.post('https://upload.uploadcare.com/base/', fileData,
                {headers: {'Content-Type': 'multipart/form-data'}}).then(
                res => {
                    console.log(res);
                    sessionStorage.setItem("imageData", JSON.stringify(res.data));
                }).then(() => {
                console.log(fileData);
                let car = {
                    brand: this.state.brand,
                    model: this.state.model,
                    number: this.state.number,
                    vin: this.state.vin,
                    year: this.state.year + "T00:00:00",
                    colour: this.state.colour,
                    type: this.state.type,
                    city: this.state.city,
                    images: this.replaceItems(),
                    userId: currentUser.id
                };
                console.log(JSON.stringify(car));
                CarService.saveCar(car).then(res => {
                    if (!res.data.error){
                        sessionStorage.removeItem("imageData");
                        this.props.history.push("/cars");
                        window.location.reload();
                    }
                }).catch(err => {
                    if (err.response == null){
                        this.setState({service: false});
                    }else if(err.response.status === 401 || sessionStorage.getItem("user") == null){
                        this.setState({resp: false});
                    }else if (err.response.status === 500){
                        this.setState({resp500: false});
                    }else if (err.response.status === 400 ){
                        this.setState({resp400: false, message: err.response.data.message});
                    }
                });
            });
        }
    };


    handleCarInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
            this.validateCarFields(name, value);
        });
    };

    state = {
        my_file: null
    }

    handleImageInput = event => {
        this.setState({
            my_file: event.target.files
        })
        console.log(event.target.files);
    }

    validateCarFields(fieldName, value) {
        let fieldCarValidationErrors = this.state.formErrors;
        let brandValid = this.state.brandValid;
        let modelValid = this.state.modelValid;
        let numberValid = this.state.numberValid;
        let vinValid = this.state.vinValid;
        let yearValid = this.state.yearValid;
        let colourValid = this.state.colourValid;
        let typeValid = this.state.typeValid;
        let cityValid = this.state.cityValid;

        switch (fieldName) {
            case "brand":
                brandValid = value.length >= 2 && value.length <= 32;
                fieldCarValidationErrors.brand = brandValid
                    ? ""
                    : "Brand is too short or empty(length from 2 to 32)";
                break;
            case "model":
                modelValid = value.length >= 2 && value.length <= 32;
                fieldCarValidationErrors.model = modelValid
                    ? ""
                    : "Model is too short or empty(length from 2 to 32)";
                break;
            case "number":
                numberValid =
                    value.match(/^[ABCEHIKMOPTX]{2}\d{4}(?<!0{4})[ABCEHIKMOPTX]{2}$/i);
                fieldCarValidationErrors.number = numberValid
                    ? ""
                    : 'Car number is invalid, example "AA1111BB" ';
                break;
            case "vin":
                vinValid =
                    value.match(/^[A-HJ-NPR-Za-hj-npr-z\d]{8}[\dX][A-HJ-NPR-Za-hj-npr-z\d]{2}\d{6}$/i);
                fieldCarValidationErrors.vin = vinValid
                    ? ""
                    : ' VIN number is invalid. VIN number format "WDB1240191J016310"';
                break;
            case "city":
                cityValid =
                    value.length > 0;
                fieldCarValidationErrors.city = cityValid
                    ? ""
                    : ' City is invalid, example "Kiev"';
                break;
            case "colour":
                colourValid = value.length >= 3 && value.length <= 30;
                fieldCarValidationErrors.colour = colourValid ? "" : ' Colour is invalid, example "Red" ';
                break;
            case "type":
                typeValid = value.length >= 3 && value.length <= 63;
                fieldCarValidationErrors.type = typeValid
                    ? ""
                    : 'Type invalid, example "Coupe"';
                break;
            case "year":
                yearValid =
                    value <= moment(new Date()).format("YYYY-MM-DD[T]HH:mm:ss") &&
                    value >=
                    moment(new Date("1900-01-01T00:00:00")).format(
                        "YYYY-MM-DD[T]HH:mm:ss"
                    );
                fieldCarValidationErrors.year = yearValid
                    ? ""
                    : " Date from 01-01-1900 to Now";
                break;
            default:
                break;
        }

        this.setState(
            {
                formErrors: fieldCarValidationErrors,
                brandValid: brandValid,
                modelValid: modelValid,
                numberValid: numberValid,
                vinValid: vinValid,
                yearValid: yearValid,
                colourValid: colourValid,
                typeValid: typeValid,
                cityValid: cityValid,
            },
            this.validateForm
        );
    }

    validateForm() {
        this.setState({
            formValid:
                this.state.brandValid &&
                this.state.modelValid &&
                this.state.numberValid &&
                this.state.vinValid &&
                this.state.yearValid &&
                this.state.colourValid &&
                this.state.typeValid &&
                this.state.cityValid,
        });
    }
    errorCarClass(error) {
        return error.length === 0 ? "" : "has-error";
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card mt-2 col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Want some money? Share your own car with others!</h3>
                            <FormErrors formErrors={this.state.formErrors}/>
                            {!this.state.resp && <p style={{ color: "red", 'margin-top': '5px' }}>Your session has expired. Please login again!</p>}
                            {!this.state.service && <p style={{ color: "red", 'margin-top': '5px' }}>Service currently unavailable.</p>}
                            {!this.state.resp500 && <p style={{ color: "red", 'margin-top': '5px' }}>Check your car data 'Number or VIN' already exists.</p>}
                            {!this.state.userHasToken && <p style={{ color: "red", 'margin-top': '5px' }}>Your session has expired. Please login again!</p>}
                            {!this.state.imagesUploaded && <p style={{ color: "red", 'margin-top': '5px' }}>Please, upload images for your car.</p>}
                            {!this.state.resp400 && <p style={{ color: "red", 'margin-top': '5px' }}>{this.state.message.toString()}</p>}
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Brand</label>
                                        <input
                                            placeholder="Enter car brand"
                                            name="brand"
                                            className={`form-control ${this.errorCarClass(
                                                this.state.formErrors.brand
                                            )}`}
                                            value={this.state.brand}
                                            onChange={this.handleCarInput}
                                        />
                                        <label>Model</label>
                                        <input
                                            placeholder="Enter car model"
                                            name="model"
                                            className={`form-control ${this.errorCarClass(
                                                this.state.formErrors.model
                                            )}`}
                                            value={this.state.model}
                                            onChange={this.handleCarInput}
                                        />
                                        <label>Number</label>
                                        <input
                                            placeholder="Enter car number"
                                            name="number"
                                            className={`form-control ${this.errorCarClass(
                                                this.state.formErrors.number
                                            )}`}
                                            value={this.state.number}
                                            onChange={this.handleCarInput}
                                        />
                                        <label>VIN</label>
                                        <input
                                            placeholder="Enter VIN number"
                                            name="vin"
                                            className={`form-control ${this.errorCarClass(
                                                this.state.formErrors.vin
                                            )}`}
                                            value={this.state.vin}
                                            onChange={this.handleCarInput}
                                        />
                                        <label>Year</label>
                                        <input
                                            type="date"
                                            name="year"
                                            min="1900-01-01"
                                            max="2021-01-01"
                                            className={`form-control ${this.errorCarClass(
                                                this.state.formErrors.year
                                            )}`}
                                            value={this.state.year}
                                            onChange={this.handleCarInput}
                                        />
                                        <label>Colour</label>
                                        <input
                                            placeholder="Enter car colour"
                                            name="colour"
                                            className={`form-control ${this.errorCarClass(
                                                this.state.formErrors.colour
                                            )}`}
                                            value={this.state.colour}
                                            onChange={this.handleCarInput}
                                        />
                                        <label>Type</label>
                                        <input
                                            placeholder="Enter car type(ex. coupe)"
                                            name="type"
                                            className={`form-control ${this.errorCarClass(
                                                this.state.formErrors.type
                                            )}`}
                                            value={this.state.type}
                                            onChange={this.handleCarInput}
                                        />
                                        <label>City</label>
                                        <input
                                            type="text"
                                            placeholder="Enter city car location"
                                            name="city"
                                            className={`form-control ${this.errorCarClass(
                                                this.state.formErrors.city
                                            )}`}
                                            value={this.state.city}
                                            onChange={this.handleCarInput}
                                        />
                                        <p style={{'margin-top': '10px'}}>
                                            <label>Car photos:</label>
                                            <form className="uploader" encType="multipart/form-data">
                                                <input name='file' multiple className='form-control' onChange={this.handleImageInput} type='file'/>
                                            </form>
                                        </p>
                                    </div>
                                    <button
                                        style={{ "margin-top": "8px" }}
                                        type="button"
                                        className="btn btn-outline-success"
                                        onClick={this.saveCar}
                                        disabled={!this.state.formValid}
                                    >
                                        Share car
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddCar;