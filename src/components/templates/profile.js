import React, { Component } from "react";
import AuthService from "../service/auth/auth-service";

class Profile extends Component{

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {firstName: '', lastName: '', middleName: '', email: '', verified: '', birthDate: '', creatTs: '', sex: '', avatarPath: ''}
    };
  }

  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();

    if (!currentUser){
      this.props.history.push('/login');
    }
    this.setState({ currentUser: currentUser})
  }

  render(){
    const { currentUser } = this.state;
    return(
      <div>
                  <div  class="card mb-3" style={{width: '650px', height: '300px', 'margin-top': '10px', 'margin-left': '400px'}}>
                    <div class="row g-0">
                      <div class="col-md-4">
                      <img src={"https://ucarecdn.com/" + currentUser.avatarPath + "/-/scale_crop/300x300/smart/"} class="img-fluid rounded-start" alt="..."/>
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">First name: {currentUser.firstName}</h5>
                          <h5 class="card-title">Last name: {currentUser.lastName}</h5>
                          <h5 class="card-title">Middle name: {currentUser.middleName}</h5>
                          <h5 class="card-title">Email name: {currentUser.email}</h5>
                          <h5 class="card-title">Account is verified: {currentUser.verified.toString()}</h5>
                          <h5 class="card-title">Birthdate: {currentUser.birthDate.substring(0, 10)}</h5>
                          <h5 class="card-title">Creation date: {currentUser.creatTs.substring(0, 10)}</h5>
                          <h5 class="card-title">Sex: {currentUser.sex}</h5>


                        </div>
                      </div>
                    </div>
                  </div>
       </div>
    )
  }
}

export default Profile;