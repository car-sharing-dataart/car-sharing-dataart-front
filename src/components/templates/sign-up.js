import React, { Component } from "react";
import userService from "../service/user-service";
import { FormErrors } from "./form-errors";
import moment from "moment";

class SingUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      middleName: "",
      birthDate: "",
      email: "",
      mobilePhone: "",
      sex: "",
      login: "",
      password: "",
      verified: "",
      creatTs: "",
      avatarPath: "",

      formErrors: {
        firstName: "",
        lastName: "",
        middleName: "",
        birthDate: "",
        email: "",
        mobilePhone: "",
        sex: "",
        login: "",
        password: "",
      },

      service: true,
      resp500: true,
      resp400: true,
      message: '',
      imagesUploaded: true,

      firstNameValid: false,
      lastNameValid: false,
      middleNameValid: false,
      birthDateValid: false,
      emailValid: false,
      mobilePhoneValid: false,
      sexValid: false,
      loginValid: false,
      passwordValid: false,
      formValid: false,
    };
    this.saveUser = this.saveUser.bind(this);
  }

  saveUser = (save) => {
    save.preventDefault();
    if(this.state.avatar == null) {
      this.setState({imagesUploaded: false});
    }else {
      userService.saveAvatar(this.state.avatar).then(() => {
        let user = {
          firstName: this.state.firstName,
          lastName: this.state.lastName,
          middleName: this.state.middleName,
          birthDate: this.state.birthDate + "T00:00:00",
          email: this.state.email,
          mobilePhone: this.state.mobilePhone,
          sex: this.state.sex,
          login: this.state.login,
          password: this.state.password,
          verified: (this.state = false),
          avatarPath: JSON.parse(localStorage.getItem("avatarData")),
          creatTs: (this.state = new Date(
              new Date().toString().split("GMT")[0] + " UTC"
          )
              .toISOString()
              .split(".")[0]),
        };
        userService.saveUser(user).then(res => {
          if (!res.data.error) {
            localStorage.removeItem("avatarData");
            this.props.history.push("/profile");
          }
        }).catch(err => {
          if (err.response == null) {
            this.setState({service: false});
          } else if (err.response.status === 500) {
            this.setState({resp500: false});
          } else if (err.response.status === 400) {
            this.setState({resp400: false, message: err.response.data.message});
          }
        });
      });
    }
  };

  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let firstNameValid = this.state.firstNameValid;
    let lastNameValid = this.state.lastNameValid;
    let middleNameValid = this.state.middleNameValid;
    let birthDateValid = this.state.birthDateValid;
    let emailValid = this.state.emailValid;
    let mobilePhoneValid = this.state.mobilePhoneValid;
    let sexValid = this.state.sexValid;
    let loginValid = this.state.loginValid;
    let passwordValid = this.state.passwordValid;

    switch (fieldName) {
      case "firstName":
        firstNameValid = value.length >= 2 && value.length <= 32 && value.match(/^[A-Z][a-z]+$/);
        fieldValidationErrors.firstName = firstNameValid
          ? ""
          : "First name must start with a capital letter(length from 2 to 32).";
        break;
      case "lastName":
        lastNameValid = value.length >= 2 && value.length <= 32 && value.match(/^[A-Z][a-z]+$/);
        fieldValidationErrors.lastName = lastNameValid
          ? ""
          : "Middle name must start with a capital letter(length from 2 to 32).";
        break;
      case "middleName":
        middleNameValid = value.length >= 2 && value.length <= 32 && value.match(/^[A-Z][a-z]+$/);
        fieldValidationErrors.middleName = middleNameValid
          ? ""
          : "Last name must start with a capital letter(length from 2 to 32).";
        break;
      case "email":
        emailValid =
          value.length <= 64 &&
          value.match(/^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/i);
        fieldValidationErrors.email = emailValid
          ? ""
          : ' Email is invalid. Email format "example@mail.com"';
        break;
      case "mobilePhone":
        mobilePhoneValid =
          value.length <= 13 && value.match(/^\+?3?8?(0\d{9})$/i);
        fieldValidationErrors.mobilePhone = mobilePhoneValid
          ? ""
          : ' Mobile phone is invalid, example "380671889999"';
        break;
      case "sex":
        sexValid = value.length > 0;
        fieldValidationErrors.sex = sexValid ? "" : " Enter your sex";
        break;
      case "login":
        loginValid = value.length >= 8 && value.length <= 32;
        fieldValidationErrors.login = loginValid
          ? ""
          : "Login is too short or empty(length from 8 to 32)";
        break;
      case "password":
        passwordValid = value.length >= 8 && value.length <= 32;
        fieldValidationErrors.password = passwordValid
          ? ""
          : " Password is too short(length from 8 to 32)";
        break;
      case "birthDate":
        birthDateValid =
          value <= moment(new Date()).format("YYYY-MM-DD[T]HH:mm:ss") &&
          value >=
            moment(new Date("1900-01-01T00:00:00")).format(
              "YYYY-MM-DD[T]HH:mm:ss"
            );
        fieldValidationErrors.birthDate = birthDateValid
          ? ""
          : " Birthdate from 01-01-1900 to 01-01-2021";
        break;
      default:
        break;
    }

    this.setState(
      {
        formErrors: fieldValidationErrors,
        firstNameValid: firstNameValid,
        lastNameValid: lastNameValid,
        middleNameValid: middleNameValid,
        birthDateValid: birthDateValid,
        emailValid: emailValid,
        mobilePhoneValid: mobilePhoneValid,
        sexValid: sexValid,
        loginValid: loginValid,
        passwordValid: passwordValid,
      },
      this.validateForm
    );
  }

  validateForm() {
    this.setState({
      formValid:
        this.state.firstNameValid &&
        this.state.lastNameValid &&
        this.state.middleNameValid &&
        this.state.birthDateValid &&
        this.state.emailValid &&
        this.state.mobilePhoneValid &&
        this.state.sexValid &&
        this.state.loginValid &&
        this.state.passwordValid,
    });
  }
  errorClass(error) {
    return error.length === 0 ? "" : "has-error";
  }

  state = {
    avatar: null
  }

  handleAvatarInput = event => {
      this.setState({
        avatar: event.target.files[0]
      })
    console.log(event.target.files[0]);
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="card mt-2 col-md-6 offset-md-3 offset-md-3">
              <h2 className="text-center">Registration</h2>
              <FormErrors formErrors={this.state.formErrors} />
              {!this.state.imagesUploaded && <p style={{ color: "red", 'margin-top': '5px' }}>Please, upload images for your profile.</p>}
              {!this.state.service && <p style={{ color: "red", 'margin-top': '5px' }}>Service currently unavailable.</p>}
              {!this.state.resp500 && <p style={{ color: "red", 'margin-top': '5px' }}>Check your data 'Email, Phone number or Login' already exists.</p>}
              {!this.state.resp400 && <p style={{ color: "red", 'margin-top': '5px' }}>{this.state.message.toString()}</p>}
              <div className="card-body">
                <form>
                  <div className="form-group">
                    <label>First name</label>
                    <input
                      placeholder="Your first name"
                      name="firstName"
                      className={`form-control ${this.errorClass(
                        this.state.formErrors.firstName
                      )}`}
                      value={this.state.firstName}
                      onChange={this.handleUserInput}
                    />
                    <label>Middle name</label>
                    <input
                      placeholder="Your middle name"
                      name="middleName"
                      className={`form-control ${this.errorClass(
                        this.state.formErrors.middleName
                      )}`}
                      value={this.state.middleName}
                      onChange={this.handleUserInput}
                    />
                    <label>Last name</label>
                    <input
                      placeholder="Your last name"
                      name="lastName"
                      className={`form-control ${this.errorClass(
                        this.state.formErrors.lastName
                      )}`}
                      value={this.state.lastName}
                      onChange={this.handleUserInput}
                    />
                    <label>Birth date</label>
                    <input
                      type="date"
                      name="birthDate"
                      min="1900-01-01"
                      max="2021-01-01"
                      className="form-control"
                      value={this.state.birthDate}
                      onChange={this.handleUserInput}
                    />
                    <label>Email</label>
                    <input
                      placeholder="Your email address (ex. expample@mail.com)"
                      name="email"
                      className={`form-control ${this.errorClass(
                        this.state.formErrors.email
                      )}`}
                      value={this.state.email}
                      onChange={this.handleUserInput}
                    />
                    <label>Phone number</label>
                    <input
                      placeholder="Your phone number (ex. 380111111111)"
                      name="mobilePhone"
                      className={`form-control ${this.errorClass(
                        this.state.formErrors.mobilePhone
                      )}`}
                      value={this.state.mobilePhone}
                      onChange={this.handleUserInput}
                    />
                    <label>Sex</label>
                    <br></br>
                    <input
                      type="radio"
                      name="sex"
                      value="MALE"
                      className="form-check-input"
                      onChange={this.handleUserInput}
                    />{" "}
                    Male
                    <br></br>
                    <input
                      type="radio"
                      name="sex"
                      value="FEMALE"
                      className="form-check-input"
                      onChange={this.handleUserInput}
                    />{" "}
                    Female
                    <br></br>
                    <label>Login</label>
                    <input
                      placeholder="Your login"
                      name="login"
                      className={`form-control ${this.errorClass(
                        this.state.formErrors.login
                      )}`}
                      value={this.state.login}
                      onChange={this.handleUserInput}
                    />
                    <label>Password</label>
                    <input
                      type="password"
                      placeholder="Your password"
                      name="password"
                      className={`form-control ${this.errorClass(
                        this.state.formErrors.password
                      )}`}
                      value={this.state.password}
                      onChange={this.handleUserInput}
                    />
                    <p style={{'margin-top': '10px'}}>
                      <label>Avatar photo:</label>
                      <form className="uploader" encType="multipart/form-data">
                        <input name='file' className='form-control' onChange={this.handleAvatarInput} type='file'/>
                      </form>
                    </p>
                  </div>
                  <button
                    style={{ "margin-top": "8px" }}
                    type="button"
                    class="btn btn-outline-success"
                    onClick={this.saveUser}
                    disabled={!this.state.formValid}
                  >
                    Sing Up
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SingUp;