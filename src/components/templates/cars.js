import React from "react";
import CarService from "../service/car-service";
import Carousel from "react-bootstrap/Carousel";

class Cars extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cars: []
        }
    }

    componentDidMount() {
        CarService.getAll().then((Response) => {
            this.setState({cars: Response.data})
        });
    }

    render() {
        return (
            <div>
                    {
                        this.state.cars.map(
                        cars =>
                            <div className="card mb-3" style={{
                                width: '750px',
                                height: '600px',
                                'margin-top': '30px',
                                'margin-left': '430px'
                            }}>
                                <div className="card">
                                    <Carousel>{ cars.images.map(({id, url}) =><Carousel.Item interval={2000}> <img key={id} src={"https://ucarecdn.com/" + url+ "/-/scale_crop/900x300/smart/"}  alt={"..."} /></Carousel.Item>)} </Carousel>
                                    <div className="row g-0">
                                        <div className="col-md-8">
                                            <div className="card-body">
                                                <h5 className="card-title">Brand: {cars.brand}</h5>
                                                    <h5 className="card-title">Model name: {cars.model}</h5>
                                                    <h5 className="card-title">Number: {cars.number}</h5>
                                                    <p className="card-text">Vin number: {cars.vin}</p>
                                                    <p className="card-text">Year: {cars.year.substring(0, 10)}</p>
                                                    <p className="card-text">Colour: {cars.colour}</p>
                                                    <p className="card-text">Type: {cars.type}</p>
                                                    <p className="card-text">City: {cars.city}</p>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        )}
            </div>
        )
    }
}

export default Cars;