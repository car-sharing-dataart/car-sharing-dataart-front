import React from "react";
import Carousel from "react-bootstrap/Carousel";
import image from "./imgs/img.png";

function Home() {
  return (
    <Carousel>
      <Carousel.Item interval={10000} style={{ height: "650px" }}>
        <img className="d-block w-100" src={image} alt="First slide" />
        <Carousel.Caption>
          <h3>New car for today</h3>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={5000} style={{ height: "650px" }}>
        <img className="d-block w-100" src={image} alt="Second slide" />
        <Carousel.Caption>
          <h3>New car for today</h3>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item style={{ height: "650px" }}>
        <img className="d-block w-100" src={image} alt="Third slide" />
        <Carousel.Caption>
          <h3>New car for today</h3>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default Home;
