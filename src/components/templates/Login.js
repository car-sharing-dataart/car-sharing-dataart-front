import React, { Component} from "react";
import AuthService from "../service/auth/auth-service";
import { FormErrors } from "./form-errors";

class Login extends Component{

    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
    
        this.state = {
          login: "",
          password: "",

            service: true,
            resp: true,
            formErrors: {login: '', password: ''},
            loginValid: false,
            passwordValid: false,
            formValid: false
        };
      }
    
    
      handleLogin(e) {
        e.preventDefault();
          AuthService.login(this.state.login, this.state.password).then(res => {
              if (!res.error){
                  this.props.history.push("/profile");
                  window.location.reload();
              }
          }).catch(err => {
              if (err.response == null){
                  this.setState({service: false});
              }else if(err.response.status === 401){
                  this.setState({resp: false});
              }
          });
        };
      
        handleUserInput = (e) => {
          const name = e.target.name;
          const value = e.target.value;
          this.setState({[name]: value},
                        () => { this.validateField(name, value) });
        }
      
        validateField(fieldName, value) {
          let fieldValidationErrors = this.state.formErrors;
          let loginValid = this.state.loginValid;
          let passwordValid = this.state.passwordValid;
      
          switch(fieldName) {
            case 'login':
              loginValid = value.length >= 8 && value.length <= 32;
              fieldValidationErrors.login = loginValid ? '' : " Login is too short or empty(length from 8 to 32)";
              break;
            case 'password':
              passwordValid = value.length >= 8 && value.length <= 32;
              fieldValidationErrors.password = passwordValid ? '': " Password is too short(length from 8 to 32)";
              break;
            default:
              break;
          }
          this.setState({formErrors: fieldValidationErrors,
                          loginValid: loginValid,
                          passwordValid: passwordValid
                        }, this.validateForm);
        }
      
        validateForm() {
          this.setState({formValid: this.state.loginValid && this.state.passwordValid});
        }
      
        errorClass(error) {
          return(error.length === 0 ? '' : 'has-error');
        }


    render() {
        return(
            <div>            
                <div className='container'>
                    <div className='row'>
                        <div className='card mt-2 col-md-6 offset-md-3 offset-md-3'>
                                <h2 className='text-center'>Log In</h2>
                                <FormErrors formErrors={this.state.formErrors} />
                                   <div className='card-body'>
                                       <form>
                                           {!this.state.resp && <p style={{ color: "red", 'margin-top': '5px' }}>Check your login or password and try again!</p>}
                                           {!this.state.service && <p style={{ color: "red", 'margin-top': '5px' }}>Service currently unavailable.</p>}
                                           <div className='form-group'>
                                               <label>Login</label>
                                               <input placeholder='Enter your login' name='login' className={`form-control ${this.errorClass(this.state.formErrors.login)}`}
                                                value={this.state.login} onChange={this.handleUserInput}/>

                                               <label>Password</label>
                                               <input type="password" placeholder='Enter your password' name='password' className={`form-control ${this.errorClass(this.state.formErrors.password)}`}
                                                value={this.state.password} onChange={this.handleUserInput}/>
                                           </div>
                                           <button style={{'margin-top': '8px'}} type="button" className="btn btn-outline-success" onClick={this.handleLogin} disabled={!this.state.formValid}>Log In</button>
                                       </form>
                                   </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Login;