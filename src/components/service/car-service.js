import axios from "axios";
import authHeader from "./auth/auth-header";

const CAR_SERVICE_API_URL = 'http://localhost:8080/cars';
const CAR_SERVICE_ADD_CAR_API_URL = 'http://localhost:8080/cars/create';

class CarService{

    getAll(){
        return axios.get(CAR_SERVICE_API_URL);
    }

    saveCar(car){
        return axios.post(CAR_SERVICE_ADD_CAR_API_URL, car, { headers: authHeader() });
    }
}

export default new CarService();