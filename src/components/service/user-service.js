import axios from "axios";

const USER_SERVICE_SIGN_UP_API_URL = 'http://localhost:8080/users/create';


class UserService{
    saveUser(user){
        return axios.post(USER_SERVICE_SIGN_UP_API_URL, user);
    }

    saveAvatar(image){
            const avatarData = new FormData();
            avatarData.append("UPLOADCARE_PUB_KEY", '7498ac6ea096b33d84d8');
            avatarData.append("UPLOADCARE_STORE", 'auto');
            avatarData.append('avatar', image);
            return axios.post('https://upload.uploadcare.com/base/', avatarData).then(
                res => {
                    console.log(res);
                    localStorage.setItem("avatarData", JSON.stringify(res).slice(18, 56));
                }
            )
    }
}

export default new UserService();