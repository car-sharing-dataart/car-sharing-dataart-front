import axios from "axios";

const API_URL = "http://localhost:8080/users/";

class AuthService {
  async login(login, password) {
    const response = await axios
      .post(API_URL + "login", {
        login,
        password
      });
    if (response.data.token) {
      sessionStorage.setItem('user',  JSON.stringify(response.data));
    }
    return response.data;
  }

  logout() {
    sessionStorage.removeItem('user');
    window.location.reload();
  }

  getCurrentUser() {
    return JSON.parse(sessionStorage.getItem('user'));
  }
}

export default new AuthService();